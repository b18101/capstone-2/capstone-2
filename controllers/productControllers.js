// [SECTION] DEPENDENCIES
const Product = require('../models/Product');

// [SECTION] CREATE PRODUCT
module.exports.createProduct = (req, res) => {

	console.log(req.body);

    Product.findOne({name: req.body.name}).then(result => {
		
		if(result !== null && result.name === req.body.name){
			
			return res.send('Product already exist.')

		} else {

			let newProduct = new Product ({
                name: req.body.name,
                description: req.body.description,
                price: req.body.price,
            });
        
            newProduct.save()
            .then(product => res.send('Product successfully created.'))
            .catch(err => res.send(err));
		}
	})
	.catch(error => res.send(error));
};

// [SECTION] RETRIEVE ALL ACTIVE PRODUCTS

module.exports.geAllProducts = (req, res) => {

    let activeProduct = {
        isActive: true
    }

    Product.find(activeProduct)
    .then(result => res.send(result))
    .catch(err => res.send(err));
};


// [SECTION] RETRIVE SINGLE PRODUCT

module.exports.getSingleProduct = (req, res) => {

    Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

// [SECTION] UPDATE PRODUCT INFORMATION

module.exports.updateProduct = (req, res) => {

    let updates = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    Product.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(updatedProduct => res.send('Product has been updated'))
    .catch(err => res.send(err));
};

// [SECTION] ARCHIVE PRODUCT

module.exports.archiveProduct = (req, res) => {

	let updates = {

		isActive: false
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedProduct => res.send('Product has been successfully archived'))
	.catch(err => res.send(err));
};
