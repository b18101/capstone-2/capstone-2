// [SECTION] DEPENDENCIES
const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcryptjs');
const auth = require('../auth');
const { reset } = require('nodemon');


// [SECTION] REGISTER USER
module.exports.registerUser = (req, res) => {

	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	User.findOne({email: req.body.email}).then(result => {
		
		if(result !== null && result.email === req.body.email){
			
			return res.send('Account already registered.')

		} else {

			let newUser = new User ({
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				mobileNo: req.body.mobileNo,
				email: req.body.email,
				password: hashedPW
			});

			newUser.save()
			.then(result => res.send('Account is now registered.'))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));

};


// [SECTION] GET ALL USER

module.exports.getAllUser = (req, res) => {

    User.find({})
    .then(result => res.send(result))
    .catch(err => res.send(err));
};


// [SECTION] LOGIN USER

module.exports.loginUser = (req, res) => {

	console.log(req.body);


	User.findOne({email: req.body.email})
	.then(foundUser => {
    
		if(foundUser === null){
			return res.send("No user found in the database")

		} else {

			const isPassWordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
			console.log(isPassWordCorrect);


			if(isPassWordCorrect){

				return res.send({accessToken: auth.createAccessToken(foundUser)})

			} else {

				return res.send("Incorrect password, please try again")
			}
		}
	})
	.catch(err => res.send(err));
};

// [SECTION] UPDATING/SETTING THE USER AS ADMIN

module.exports.updateUserDetails = (req, res) => {

	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => {
        res.send('Account successfully updated to admin')})
	.catch(err => res.send(err));

};


// [SECTION] CREATE ORDER BY USER

module.exports.createUserOrder = async (req, res) => {

	// Checking if a user is an admin, if he is an admin then he should not be able to enroll
	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	}

	let isUserOrderCreated = await User.findById(req.user.id).then(user => {

		let newOrder = {
			productId: req.params.id,
			name: req.body.name,
			totalAmount: req.body.totalAmount
		}

		user.orders.push(newOrder)

		return user.save()
		.then(user => true)
		.catch(err => err.message)
	})

	if(isUserOrderCreated !== true){
		return res.send({message: isUserOrderCreated})
	}

	let isProductOrderCreated = await Product.findById(req.params.id).then(product => {

		let purchaseDetails = {
			userId: req.user.id
		}

		product.buyer.push(purchaseDetails)

		return product.save()
		.then(product => true)
		.catch(err => err.message)
	})

	if(isProductOrderCreated !== true){

		return res.send({message: isProductOrderCreated})
	}

	// if(isUserOrderCreated && isProductOrderCreated){
	// 	return res.send({message: 'Order successfully placed.'})
	// }


	if(isUserOrderCreated && isProductOrderCreated){

		Product.findOne({_id: req.params.id})
			.then((result) => {
				res.send(`Your ${req.body.totalAmount} ${result.name} order has been placed successfully.`)
			})
			.catch(err => res.send(err));
	}

};

// [SECTION] RETRIEVE ORDER(S) FROM AUTHENTICATED USER

module.exports.getAllUserOrders = (req, res) => {

	User.findById(req.user.id)
	.then(result => res.send(result.orders))
	.catch(err => res.send(err));
};

// [SECTION] RETRIVE ALL ORDERS BY ADMIN

module.exports.getAllOrders = (req, res) => {

	let arr = [];
	
	User.find({})
		.then((result) => {
			result.forEach((item) => {
				if (item.orders.length !== 0) {
					arr.push({
						_id: item._id,
						orders: item.orders
					})
				}
			})
			res.send(arr)	
		})
		.catch(err => res.send(err));
};