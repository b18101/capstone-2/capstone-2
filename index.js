// [SECTION] DEPENDENCIES
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// [SECTION] SERVER
const app = express();
const port = 4000;

// [SECTION] DATABASE CONNECTION
mongoose.connect("mongodb+srv://admin:Rdpc0610@wdc028-course-booking.ohnls.mongodb.net/capstone-2?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("Successfully connected to MongoDB"));

// [SECTION] MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


// [SECTION] GROUP ROUTING

const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

const productRoutes = require('./routes/productRoutes');
app.use('/products', productRoutes);

// [SECTION] PORT LISTENER
app.listen(port, () => console.log(`Server is running at port ${port}`));