const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "product name is required"]
	},

	description: {
		type: String,
		required: [true, "product description is required"]
	},

	price: {
		type: Number,
		required: [true, "product price is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	}, 

	createdOn: {
		type: Date,
		default: new Date()
	},

	buyer: [
		{
			userId: {
				type: String,
				required: [true, 'user ID cannot be empty']
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);