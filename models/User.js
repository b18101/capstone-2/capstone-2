const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({


	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	
	mobileNo: {
		type: Number,
		required: [true, "Mobile number is required"]
	},

	email: {
		type: String,
		required: [true, "User's email is required"]
	},

	password: {
		type: String,
		required: [true, "User's password is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	orders: [
		{
			productId: {
				type: String,
				required: [true, 'product ID cannot be empty'],
			},

			name: {
				type: String,
				required: [true, "product name is required"]
			},

			totalAmount: {
				type: Number
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]

});

module.exports = mongoose.model("User", userSchema);