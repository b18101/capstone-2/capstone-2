// [SECTION] DEPENDENCIES
const express = require('express');
const router = express.Router();

// [SECTION] IMPORTED MODULES
const productControllers = require('../controllers/productControllers');
const auth = require('../auth');

const {verify, verifyAdmin} = auth;

// [SECTION] ROUTES

// create product
router.post('/createProduct', verify, verifyAdmin, productControllers.createProduct);

// get all products
router.get('/getAllProducts', productControllers.geAllProducts);

// get single product
router.get('/getSingleProduct/:id', productControllers.getSingleProduct);

// update product information
router.put('/updateProduct/:id', verify, verifyAdmin, productControllers.updateProduct);

// archive product
router.put('/archiveProduct/:id', verify, verifyAdmin, productControllers.archiveProduct);

module.exports = router;
