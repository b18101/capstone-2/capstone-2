// [SECTION] DEPENDENCIES
const express = require('express');
const router = express.Router();

// [SECTION] IMPORTED MODULES
const userControllers = require('../controllers/userControllers');
const auth = require('../auth');

//object destructuring from auth module
const { verify, verifyAdmin } = auth;

// [SECTION] ROUTES

// register user
router.post('/registerUser', userControllers.registerUser);

// get user
router.get('/', userControllers.getAllUser)

// login user

router.post('/loginUser', userControllers.loginUser);

// set user as admin
router.put('/updateUserDetails/:id', verify, verifyAdmin, userControllers.updateUserDetails);

// create user order
router.post('/createUserOrder/:id', verify, userControllers.createUserOrder);

// retrieve user's order(s)
router.get('/getAllUserOrders', verify, userControllers.getAllUserOrders);

// retrive all orders (admin only)
router.get('/getAllOrders', verify, verifyAdmin, userControllers.getAllOrders);

module.exports = router;
